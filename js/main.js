"use strict";
/* 0. Initialization */
// Get height on Window resized
$(window).on('resize',function(){
    var slideHeight = $('.slick-track').innerHeight();
	return false;
});


// Smooth scroll <a> links 
var $root = $('html, body');
$('a.s-scroll').on('click',function() {
    var href = $.attr(this, 'href');
    $root.animate({
        scrollTop: $(href).offset().top
    }, 500, function () {
        window.location.hash = href;
    });
    return false;
});

// Page Loader : hide loader when all are loaded
$(window).load(function(){
    $('#page-loader').addClass('hidden');
});


/* 1. Clock attribute */

var dateReadableText = 'Upcoming date';
    if($('.site-config').attr('data-date-readable') && ($('.site-config').attr('data-date-readable') != '')){
        $('.timeout-day').text('');
        dateReadableText = $('.site-config').attr('data-date-readable');        
        $('.timeout-day').text(dateReadableText);
    }
$('.clock-countdown').downCount({
    date: $('.site-config').attr('data-date'),
    offset: +10
}, function () {
    //callback here if finished
    //alert('YES, done!');
    var zerodayText = 'An upcoming date';
    if($('.site-config').attr('data-zeroday-text') && ($('.site-config').attr('data-zeroday-text') != '')){
        $('.timeout-day').text('');
        zerodayText = $('.site-config').attr('data-zeroday-text'); 
    }
    $('.timeout-day').text(zerodayText);
});


/* 2. Background for page / section */

var background = '#ccc';
var backgroundMask = 'rgba(255,255,255,0.92)';
var backgroundVideoUrl = 'none';

/* Background image as data attribut */
var list = $('.bg-img');

for (var i = 0; i < list.length; i++) {
	var src = list[i].getAttribute('data-image-src');
	list[i].style.backgroundImage = "url('" + src + "')";
	list[i].style.backgroundRepeat = "no-repeat";
	list[i].style.backgroundPosition = "center";
	list[i].style.backgroundSize = "cover";
}

/* Background color as data attribut */
var list = $('.bg-color');
for (var i = 0; i < list.length; i++) {
	var src = list[i].getAttribute('data-bgcolor');
	list[i].style.backgroundColor = src;
}

/* Background slide show Background variables  */
var imageList = $('.slide-show .img');
var imageSlides = [];
for (var i = 0; i < imageList.length; i++) {
	var src = imageList[i].getAttribute('data-src');
	imageSlides.push({src: src});
}


/* Slide Background variables */
var isSlide = false;
var slideElem = $('.slide');
var arrowElem = $('.p-footer .arrow-d');
var pageElem = $('.page');

/* 3. Init all plugin on load */
$(document).ready(function() {
	/* Init console to avoid error */
	var method;
    var noop = function () {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
	
	/* Init Slidesow background */
	 $('.slide-show').vegas({
        delay: 5000,
        shuffle: true,
        slides: imageSlides,
    	//transition: [ 'zoomOut', 'burn' ],
		animation: [ 'kenburnsUp', 'kenburnsDown', 'kenburnsLeft', 'kenburnsRight' ]
    });
	
	/* Init video background */
	$('.video-container video, .video-container object').maximage('maxcover');
	
	/* Init youtube video background */
	if(backgroundVideoUrl != 'none'){
        
        //disable video background for smallscreen
        if($(window).width() > 640){
          $.okvideo({ source: backgroundVideoUrl,
                    adproof: true
                    });
        }
    }
	
	/** Init fullpage.js */
    $('#mainpage').fullpage({
		menu: '#qmenu',
		anchors: ['home',  'register', 'about-us', 'contact'],
//        verticalCentered: false,
//        resize : false,
//		responsive: 900,
		scrollOverflow: true,
        css3: false,
        navigation: true,
		onLeave: function(index, nextIndex, direction){
			arrowElem.addClass('gone');
			pageElem.addClass('transition');
//			$('.active').removeClass('transition');
			slideElem.removeClass('transition');
			isSlide = false;
		},
        afterLoad: function(anchorLink, index){
			arrowElem.removeClass('gone');
			pageElem.removeClass('transition');
			if(isSlide){
				slideElem.removeClass('transition');
			}
		},
		
        afterRender: function(){}
    });
});

var Eckes={};
// 弹出 全局 消息框
Eckes.msg = function (opt) {
    var text = opt.text || "未传入提示语！";
    var time = opt.time || 2000;
    var icon = opt.icon || "fa-info-circle";

    //创建最外层容器
    var msg_layout = document.getElementsByClassName("eckes_message_layout");
    if (msg_layout.length == 0) {

        var styler = document.createElement('style');
        styler.type = "text/css";
        styler.innerHTML = `
  .eckes_message_layout{
      position:fixed;top:24px;left:0;
      width:100%;pointer-events:none;
      font-size:14px;z-index:1024;
  }
  .eckes_message_box{
      transition:all .3s ease-in-out;
      text-align:center;
      padding:8px;
      transform:translateY(-25%);
      opacity:0;
  }
  .eckes_message_box.in{ transform:translateY(0);opacity:1; }
  .eckes_message_content{
      position:relative;
      display: inline-block;
      pointer-events: all;
      padding: 0 10px 0 24px;
      border-radius: 4px;
      box-shadow: 0 1px 6px rgba(0,0,0,.2);
      background: #fff;
      height:2.5em;
      line-height: 2.5em;/* 35/14 */
  }
  .eckes_message_content>.fa{
      position:absolute;
      left:6px;
      height:2.5em;
      line-height: 2.5em;
      top:0;
  }
  .eckes_message_content.noIcon{padding: 0 10px;}
  .eckes_message__loading{text-align:center;width:65px;height:45px;}
  .eckes_message_content.eckes_message__loading>.fa{position:unset;color: rgb(48, 163, 240);font-size:18px;}
  .eckes_message_content .icon-info{ color: rgb(48, 163, 240); }
  .eckes_message_content .icon-error{ color: rgb(255, 17, 69); }
  .eckes_message_content.eckes_message__error{ box-shadow: 0 1px 6px rgba(255, 0, 0, 0.95);}
  .eckes_message_content .icon-warning{ color: rgb(255, 170, 11); }
  .eckes_message_content .icon-default{ color: rgb(161, 161, 161); }
`;

        msg_layout = document.createElement("div");
        msg_layout.className = "eckes_message_layout";
        document.body.appendChild(styler);
        document.body.appendChild(msg_layout);
    } else {
        msg_layout = msg_layout[0]
    }
    //创建单条message的box
    var msg_box = document.createElement("div");
    msg_box.className = "eckes_message_box " + (opt.cls ? "eckes_message_box__" + opt.cls : "");
    //创建message
    var msg_content = document.createElement("div");
    msg_content.className = "eckes_message_content " + (opt.cls ? "eckes_message__" + opt.cls : "");
    if (opt.cls == "loading") {
        var msg_icon = document.createElement("i");
        msg_icon.className = "fa fa-spin fa-spinner";
        msg_content.appendChild(msg_icon);
    } else {
        msg_content.innerText = text;
    }
    //创建message的icon
    if (!opt.noIcon) {
        var msg_icon = document.createElement("i");
        msg_icon.className = "fa " + icon + " icon-" + (opt.type || "info");
        msg_content.appendChild(msg_icon);
    } else {
        msg_content.classList.add("noIcon");
    }

    msg_box.appendChild(msg_content);
    msg_layout.appendChild(msg_box);
    //到时间隐藏并删除message box
    setTimeout(() => {
        msg_box.classList.add("in");
        setTimeout(() => {
            msg_box.classList.remove("in");
            setTimeout(() => {
                msg_box && msg_layout.removeChild(msg_box);
            }, 300)
        }, time)
    }, 100)
}
Eckes.info = function (text, type, time) {
    var opt = {};
    if (typeof text == "object") {
        opt = text;
    } else {
        opt = {
            text: text || "",
            time: time || "",
            type: type || "info"
        }
    }
    opt.text = opt.text || "";
    opt.time = opt.time || "";
    opt.type = opt.type || "info";
    if (opt.type == "error") {
        opt.icon = "fa-times-circle";
        opt.time = 3000;
        opt.cls = "error";
    }
    this.msg(opt)
};
Eckes.loading = function (remove) {
    var l = document.querySelector(".eckes_message_box__loading");
    if (remove) {
        if(!l)return;
        var index = l.getAttribute("data-index");
        if(!index || index<=0){
            l.classList.remove("in");
            setTimeout(() => {
                l.parentNode && l.parentNode.removeChild(l);
            }, 300)
        }else{
            l.setAttribute("data-index",+index-1);
            console.log(index);
        }
        return;
    }
    if (l){
        var index = l.getAttribute("data-index");
        if(!index){
            l.setAttribute("data-index",1);
        }else{
            l.setAttribute("data-index",+index+1);
        }
        return;
    }
    this.msg({
        cls: "loading",
        noIcon: true,
        time: 3000000
    })
};